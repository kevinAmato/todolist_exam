const todoServices = require("../services/todos");

//controleur de la route GET /todos
exports.list = async (req, res) => {
  const id = req.params.id;
  return await todoServices
    .list(id)
    .then((data) => {
      const formatedData = formatData(data);
      res.json(formatedData);
    })
    .catch((e) => res.send(`Error : ${e}`));
};

exports.postItem = async (req, res) => {
  return await todoServices
    .create(req.body)
    .then((data) => {
      res.json(data);
    })
    .catch((e) => res.send(`Error : ${e}`));
};

exports.deleteItem = async (req, res) => {
  const id = req.params.id;

  return await todoServices
    .delete(id)
    .then((deleted) => {
      res.json(deleted);
    })
    .catch((e) => res.send(`Error : ${e}`));
};

exports.updateItem = async (req, res) => {
  const id = req.params.id;
  const todo = req.body;

  return await todoServices
    .update(id, todo)
    .then((updated) => {
      res.json(updated);
    })
    .catch((e) => res.send(`Error : ${e}`));
};

function formatData(listTodos) {
  const output = [];

  if (Array.isArray(listTodos)) {
    listTodos.forEach((listTodo) => {
      output.push({
        id: listTodo._id.valueOf(),
        done: listTodo.done,
        description: listTodo.description,
      });
    });
  } else {
    output.push({
      id: listTodos._id.valueOf(),
      done: listTodos.done,
      description: listTodos.description,
    });
  }

  return output;
}
