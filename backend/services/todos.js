const TodoModel = require("../models/Todos");

exports.list = async (id) => {
  if (!id || id === "") {
    return await TodoModel.find();
  } else {
    return await TodoModel.findById(id);
  }
};

exports.create = async (req) => {
  const todo = await TodoModel.create(req);
  return todo;
};

exports.delete = async (id) => {
  return await TodoModel.deleteOne({ _id: id });
};

exports.update = async (id, todo) => {
  return await TodoModel.updateOne({ _id: id }, todo);
};
