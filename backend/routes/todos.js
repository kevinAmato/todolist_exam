const express = require("express");
const router = express.Router();

const todoCtrl = require("../controllers/todos");

router.post("/", todoCtrl.postItem);
router.get("/", todoCtrl.list);
router.get("/:id", todoCtrl.list);
router.patch("/:id", todoCtrl.updateItem);
router.delete("/:id", todoCtrl.deleteItem);
router.put("/:id", todoCtrl.updateItem);

module.exports = router;
