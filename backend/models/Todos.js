const mongoose = require("mongoose");

const { Schema } = mongoose;

const todoSchema = new Schema({
  description: { type: String },
  done: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("todo", todoSchema);
