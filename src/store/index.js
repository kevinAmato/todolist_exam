import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

import config from "@/config/page";

// Create a new store instance.
const store = new Vuex.Store({
  state: {
    page: 1,
    todos: [],
  },
  getters: {
    paginatedTodos(state) {
      return state.todos.slice(
        (state.page - 1) * config.PAGE_SIZE,
        (state.page - 1) * config.PAGE_SIZE + config.PAGE_SIZE
      );
    },
    percentageDone(state) {
      return `${
        state.todos.length > 0
          ? (
              (state.todos.filter((todo) => todo.done).length /
                state.todos.length) *
              100
            ).toFixed(2)
          : 0
      }%`;
    },
    newId() {
      return Date.now();
    },
  },
  mutations: {
    deleteTodo(state, id) {
      axios.delete(`http://localhost:3000/api/v1/todos/${id}`).then((res) => {
        this.commit('setTodos')
        return res;
      });
    },
    addTodo(state, todo) {
      if (todo) {
        if (todo.description === "") {
          todo.description = `blank`;
        }
        axios.post("http://localhost:3000/api/v1/todos", todo).then((res) => {
          this.commit('setTodos')
          return res;
        });
      }
    },
    editTodo(state, updatedTodo) {
      axios
        .put(`http://localhost:3000/api/v1/todos/${updatedTodo.id}`, updatedTodo)
        .then((res) => {
          this.commit('setTodos')
          return res;
        });
    },
    toggleDone(state, id) {
      const todoToEdit = state.todos.find((todo) => todo.id === id);
      if (todoToEdit) {
        todoToEdit.done = !todoToEdit.done;
      }
      axios.put(`http://localhost:3000/api/v1/todos/${todoToEdit.id}`, todoToEdit).then((res) => {
        this.commit('setTodos')
        return res
      })
    },
    selectPage(state, page) {
      state.page = page;
    },
    setTodos(state) {
      axios.get("http://localhost:3000/api/v1/todos").then((res) => {
        state.todos = res.data;
        (res.data)
      });
    },
  },
  actions: {
    deleteTodo({ commit }, id) {
      commit("deleteTodo", id);
    },
    addTodo({ commit, getters }, description) {
      commit("addTodo", {
        description,
        id: getters.newId
      });
    },
    editTodo({ commit }, todo) {
      commit("editTodo", todo);
    },
    toggleDone({ commit }, id) {
      commit("toggleDone", id);
    },
    selectPage({ commit }, page) {
      commit("selectPage", page);
    },
    getDbTodos({ commit }) {
      commit("setTodos");
    },
  },
});

export default store;
